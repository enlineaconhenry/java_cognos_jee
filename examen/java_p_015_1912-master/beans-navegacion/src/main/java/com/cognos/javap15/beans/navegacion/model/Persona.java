/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.beans.navegacion.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
 *
 * @author JAVA
 */
@Named
@SessionScoped
public class Persona implements Serializable{
 
    private String nombre;
    private String apellido;
    private int edad;
    private String sexo;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
    
    public List<String> validar(){
        List<String> errores = new ArrayList<String>();
        if(nombre.length() <= 2){
            errores.add("Nombre incrrecto");
        }
        if(apellido.length() <= 2){
            errores.add("Apellido incrrecto");
        }
        return errores;    
    }
    
}
