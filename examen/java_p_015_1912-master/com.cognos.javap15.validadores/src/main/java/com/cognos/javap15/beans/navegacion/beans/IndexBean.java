/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.beans.navegacion.beans;

import com.cognos.javap15.beans.navegacion.bl.DepartamentoBL;
import com.cognos.javap15.beans.navegacion.model.Departamento;
import com.cognos.javap15.beans.navegacion.model.Persona;
import com.cognos.javap15.beans.navegacion.util.Cttes;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedType;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Any;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.util.AnnotationLiteral;
import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlInputText;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author JAVA
 */
@Named
@RequestScoped
public class IndexBean {

    @Inject
    Persona persona;

    @Inject
    BeanManager beanManager;
    
    private List<String> errores;
    private List<Departamento> deps;
    private Departamento depSeleccionado;

    @PostConstruct
    public void init() {
        deps = DepartamentoBL.obtenerDeps();
        
        Set<Bean<?>> beans = beanManager.getBeans(Object.class, new AnnotationLiteral<Any>() {});
        
        //Set<Bean<?>> beans = beanManager.getBeans(Object.class, IndexBean.class.getAnnotations());
        
       
        beans.stream()
                .filter(a -> a.toString().contains("cognos"))
                //.filter(a -> isNamed(a.getBeanClass()))
                .filter(a -> a.getBeanClass().isAnnotationPresent(Named.class))
                .forEach(a -> System.out.println("----" + a.toString()));
    }

    public boolean isNamed(Class<?> bean){
        
        System.out.println(bean.getClass());
                
//        Arrays.asList(bean.getClass().getAnnotations())
//                .stream()
//                .forEach(b -> System.out.println(b.getClass()));
               // .filter(b -> (b.getClass() == Named.class))
               
                //.toArray().length > 0;
        return  true;
         
    }
    
    
    public String procesar() {

        System.out.println("Ingresando al metodo");
        validar();
        if (errores != null && errores.size() > 0) {
            return null;
        } else {
            return Cttes.PAGE_PERFIL;
        }
    }

    private void validar() {
        errores = persona.validar();
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public List<String> getErrores() {
        return errores;
    }

    public void setErrores(List<String> errores) {
        this.errores = errores;
    }

    public void cambiarIdioma() {
        System.out.println("Ingresando a cambiar idioma.....");
        FacesContext.getCurrentInstance().getViewRoot().setLocale(Locale.forLanguageTag("es"));
    }

    public List<Departamento> getDeps() {
        return deps;
    }

    public void setDeps(List<Departamento> deps) {
        this.deps = deps;
    }

    public Departamento getDepSeleccionado() {
        return depSeleccionado;
    }

    public void setDepSeleccionado(Departamento depSeleccionado) {
        this.depSeleccionado = depSeleccionado;
    }

    public void cambiarNick(ValueChangeEvent ve) {
        System.out.println("ingresando a cambiarNick " + ve.getNewValue());
        UIViewRoot uIViewRoot = FacesContext.getCurrentInstance().getViewRoot();
        String valor = ve.getNewValue().toString();
        HtmlInputText itxNick = (HtmlInputText) uIViewRoot.findComponent("frmPrincipal:itxNick");
        itxNick.setValue(valor);
        itxNick.setSubmittedValue(valor);
        FacesContext.getCurrentInstance().renderResponse();
    }
    
    public void cambiarNickAjax(){
        System.out.println(".............. Ingresando a metodo NickAjax");
        persona.setNick(persona.getApellido());
    }

}
