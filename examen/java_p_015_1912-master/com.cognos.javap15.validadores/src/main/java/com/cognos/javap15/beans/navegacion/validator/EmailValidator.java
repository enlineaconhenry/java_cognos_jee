/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.beans.navegacion.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author JAVA
 */

@FacesValidator("emailValidator")
public class EmailValidator implements Validator<String>{

    @Override
    public void validate(FacesContext fc, UIComponent uic, String t) throws ValidatorException {
        if(!t.contains("@")){
            FacesMessage msg = new FacesMessage("Error email", "Formato incorrecto");
            throw new ValidatorException(msg);
        }
    }
    
}
