/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.beans.navegacion.general;

import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlInputText;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;


/**
 *
 * @author JAVA
 */
public class CicloVidaListener implements PhaseListener{
    
    @Override
    public void beforePhase(PhaseEvent pe) {
        System.out.println("Antes de la fase " + pe.getPhaseId());        
    }
      

    @Override
    public void afterPhase(PhaseEvent pe) {
      System.out.println("Despuès de la fase " + pe.getPhaseId());     
    }  

    @Override
    public PhaseId getPhaseId() {
       return PhaseId.ANY_PHASE;
    }
    
}
