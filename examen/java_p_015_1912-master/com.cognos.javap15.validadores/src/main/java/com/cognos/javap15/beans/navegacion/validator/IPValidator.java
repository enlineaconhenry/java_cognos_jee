/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.beans.navegacion.validator;

import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author JAVA
 */

@FacesValidator("ipValidator")
public class IPValidator implements Validator<String>{

    @Override
    public void validate(FacesContext fc, UIComponent uic, String t) throws ValidatorException {
        String regular = "^(?:[0-9]{1,3}\\.){3}[0-9]{1,3}$";
        if(!Pattern.matches(regular, t)){
            FacesMessage msg = new FacesMessage("Error ip", "Formato ip incorrecto");
            throw new ValidatorException(msg);
        }
    }
    
}
