/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hcoarite.practicas.examen;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author Domo
 */
@FacesValidator("textValidator")
public class TextValidator implements Validator<String>{

    @Override
    public void validate(FacesContext fc, UIComponent uic, String t) throws ValidatorException {
        
         if(t==null||t.length()<=3){
            FacesMessage msg = new FacesMessage("Debe de introducir mas de 3 caracteres");
            throw  new ValidatorException(msg);
        }
    }
    
}
